/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "../LIBS/WS2813.h"
#include "x_nucleo_iks01a2.h"
#include "x_nucleo_iks01a2_accelero.h"
#include "usart.h"
//#define KARR
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId defaultTaskHandle;

/* USER CODE BEGIN Variables */
#define LA_SIZE_MAX 40
static char dataOut[LA_SIZE_MAX];
static void *LSM6DSL_X_0_handle = NULL;
/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
// Acquisition pour mise à jour des LEDS
void vAcquisitionCallback(TimerHandle_t timer);
// Affichage sur CuteCom
void vAffichageCallback(TimerHandle_t timer);
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
  ws2813_init_tim(&htim3, TIM_CHANNEL_1, 8);
  BSP_ACCELERO_Init( LSM6DSL_X_0, &LSM6DSL_X_0_handle );
  BSP_ACCELERO_Sensor_Enable( LSM6DSL_X_0_handle );
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */

  // Timer d'application
  TimerHandle_t applicationTimer = xTimerCreate("Acquisition",
  		  	  pdMS_TO_TICKS (50),
  			  pdTRUE,
  			  NULL,
  			  vAcquisitionCallback);
    xTimerStart(applicationTimer, pdMS_TO_TICKS(10));


    // Timer pour l'affichage
    TimerHandle_t cuteComTimer = xTimerCreate("Affichage",
      		  	  pdMS_TO_TICKS (500),
      			  pdTRUE,
      			  NULL,
      			  vAffichageCallback);
    xTimerStart(cuteComTimer, pdMS_TO_TICKS(10));

  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Application */

void vAffichageCallback(TimerHandle_t timer){
	 uint8_t id;
	 SensorAxes_t acceleration;
	 uint8_t status;

	BSP_ACCELERO_Get_Instance( LSM6DSL_X_0_handle, &id );

	BSP_ACCELERO_IsInitialized( LSM6DSL_X_0_handle, &status );

	// Est-ce que l'accelero est initialisé ?
	if ( status == 1 )
	  {
	    if ( BSP_ACCELERO_Get_Axes( LSM6DSL_X_0_handle, &acceleration ) == COMPONENT_ERROR )
	    {
	    	snprintf( dataOut, LA_SIZE_MAX, "ERREUR INITIALISATION ! \n");
	    	HAL_UART_Transmit( &huart2, ( uint8_t * )dataOut, strlen( dataOut ), 5000 );

	    } else {
	    
	    	snprintf( dataOut, LA_SIZE_MAX, "\r INCLINAISON = %d\r\n", (int)acceleration.AXIS_X);
	    	HAL_UART_Transmit( &huart2, ( uint8_t * )dataOut, strlen( dataOut ), 5000 );
	    }
	  }

}


// Fonction appelée par le timer
// La fonction est appelée toutes les 100ms
// Dans cette fonction on va détecter l'état de l'accélero et avec ses valeurs
// on va pouvoir changer l'affichage des LED suivant des codes couleurs :
// vert - bleu - orange - rouge
void vAcquisitionCallback(TimerHandle_t timer){

	/* VARIABLES GLOBALES ACCELERO */
	  uint8_t id;
	  SensorAxes_t acceleration;
	  uint8_t status;
	  int accelerationValue = 0;

	/* VARIABLES GLOBALES LEDS */

	  WS2813_COLOR color_on={.rgb={.r=0,.g=0,.b=0}}; // Utilisation du RGB
	  int i;
	  int numLed;

      /************** PROGRAMME ACCELERO AXE X *******************/
	  BSP_ACCELERO_Get_Instance( LSM6DSL_X_0_handle, &id );

	  BSP_ACCELERO_IsInitialized( LSM6DSL_X_0_handle, &status );

	  // Est-ce que l'accelero est initialisé ?
	  if ( status == 1 )
	  {
	    if ( BSP_ACCELERO_Get_Axes( LSM6DSL_X_0_handle, &acceleration ) == COMPONENT_ERROR )
	    {
	      // Toutes les LED ROUGE si accelero non initialisé
	      for ( i = 0; i < 8; i++){
	    	  color_on.rgb.r = 63;
	    	  color_on.rgb.b = 0;
	    	  color_on.rgb.g = 0;

	    	  ws2813_update_led(i, color_on); // Update des couleurs LED par LED
	      }
	      ws2813_start_transmission_tim();

	      return;

	    }

	    // Attribution à la valeur globale pour l'utiliser dans les LED
	    accelerationValue = (int)acceleration.AXIS_X;
	  }

	  /************** PROGRAMME LED *******************/

	  // LED A ALLUMER SELON LA VALEUR DE LA VALEUR GLOBALE
	  // 
	  if(accelerationValue <= -200){
		  numLed = 0;
	  }else if (accelerationValue > -200 && accelerationValue <= -100){
		  numLed = 1;
	  }else if (accelerationValue > -100 && accelerationValue <= -40){
		  numLed = 2;
	  }else if (accelerationValue > -40 && accelerationValue <= -10){
 		  numLed = 3;
	  }else if (accelerationValue > 10 && accelerationValue <= 40){
		  numLed = 4;
	  }else if (accelerationValue > 40 && accelerationValue <= 100){
		  numLed = 5;
	  }else if (accelerationValue > 100 && accelerationValue <= 200){
		  numLed = 6;
	  }else if (accelerationValue > 200){
		  numLed = 7;
	  }else if (accelerationValue < 10 && accelerationValue > -10){
		  numLed = 8;
	  }


	  // Pour toutes les valeurs décidées
	  for (i = 0; i < 9; i++){

		  if ( i == numLed) {
			  
			  // On allume en bleu les LED du milieu
			  if(numLed == 8)
			  {
				  color_on.rgb.r = 0;
				  color_on.rgb.g = 0;
				  color_on.rgb.b = 255;
				  ws2813_update_led(3, color_on); // Met à jour avec dans WS2813
				  ws2813_update_led(4, color_on);
			  }

			 // Turquoise sur la LED voulue
			  if ( numLed == 3 || numLed == 4)
			  { 
				  color_on.rgb.g = 254;
				  color_on.rgb.b = 95;
				  ws2813_update_led(i, color_on);
			  } 
			  // Vert sur la LED voulue
			  else if ( numLed == 2 || numLed == 5 )
			  {
				  color_on.rgb.g = 63;
				  ws2813_update_led(i, color_on);
			  }
			  // On passe au Orange
			  else if ( numLed == 1 || numLed == 6)
			  {
				  color_on.rgb.r = 247;
				  color_on.rgb.g = 181;
				  color_on.rgb.b = 0;
				  ws2813_update_led(i, color_on);
				  
			  }
			  else // On est sur les LED aux extremités, en rouge
			  {
				  color_on.rgb.r = 63;
				  color_on.rgb.g = 0;
				  color_on.rgb.b = 0;
				  ws2813_update_led(i, color_on);
			  }
			  // Les autres ne sont pas allumées
		  } else {
			  color_on.rgb.r = 0;
			  color_on.rgb.b = 0;
			  color_on.rgb.g = 0;
			  ws2813_update_led(i, color_on);
		  }

	  }

	  // Transmission
	  ws2813_start_transmission_tim();
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
